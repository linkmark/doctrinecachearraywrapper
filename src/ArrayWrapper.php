<?php

namespace Linkmark\Doctrine\Cache;

use ArrayAccess;
use Doctrine\Common\Cache\Cache;

class ArrayWrapper implements Cache, ArrayAccess
{
    /**
     * Doctrine 2 cache implementation.
     *
     * @var Cache
     */
    private $cache;

    /**
     * Class constructor with required dependency.
     *
     * @param Cache $cache Doctrine 2 cache instance.
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch($id)
    {
        return $this->cache->fetch($id);
    }

    /**
     * {@inheritdoc}
     */
    public function save($id, $data, $lifeTime = 0)
    {
        return $this->cache->save($id, $data, $lifeTime);
    }

    /**
     * {@inheritdoc}
     */
    public function contains($id)
    {
        return $this->cache->contains($id);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        return $this->cache->delete($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getStats()
    {
        return $this->cache->getStats();
    }

    /**
     * Doctrine 2 to PHP array: array offset access
     *
     * @param mixed $offset The offset to retrieve.
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->cache->fetch($offset);
    }

    /**
     * Doctrine 2 to PHP array: array offset definition
     *
     * @param mixed $offset The offset to assign the value to.
     * @param mixed $value  The value to set.
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        return $this->cache->save($offset, $value);
    }

    /**
     * Doctrine 2 to PHP array: isset()
     *
     * @param mixed $offset An offset to check for.
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return $this->cache->contains($offset);
    }

    /**
     * Doctrine 2 to PHP array: unset()
     *
     * @param mixed $offset The offset to unset.
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        return $this->cache->delete($offset);
    }
}
